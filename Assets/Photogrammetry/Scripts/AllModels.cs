﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AllModels {
    public List<ModelData> Models;
}
